module comparator_tb;

	
	reg [7:0] DOut1;
	reg [7:0] DOut2;
	wire sign;
	
comparator comp (.DOut2(DOut2),.DOut1(DOut1),.sign(sign));
	
	initial
	 begin
		DOut1<=0; DOut2<=0;
	#5
		DOut1 <= 5;
		DOut2 <= 8;
	#5
		DOut1 <= 3;
		DOut2 <= 1;
	#5
		DOut1 <= 10;
		DOut2 <= 11;
	#5
		DOut1 <= 9;
		DOut2 <= 2;
	#5
		DOut1 <= 5;
		DOut2 <= 5;
	end

 
endmodule

module CounterA(IncA,Reset,clock,AddrA);
	
	input IncA, Reset, clock;
	output [2:0] AddrA;
	reg [2:0] AddrA;
	
	//initial
	//AddrA <= 3'b0;

always @(posedge clock) 
begin
	if (Reset == 1)
		begin	
		AddrA <= 3'b0;
		end

	else if (IncA == 1 && Reset == 0)
		begin
		AddrA <= AddrA + 1;
		end
	else 
		begin
			AddrA<=3'b000;
	end
end
endmodule 

module ADD( AddOut,DOut1,DOut2);

	output [7:0]AddOut;
	input [7:0] DOut1;
	input [7:0] DOut2;
	reg [7:0] AddOut;
	

always @(DOut1,DOut2)
 begin	
  AddOut= DOut1+DOut2;
 end

endmodule







module M_M(WEB,WEA,IncA,IncB,sign,clock,Reset,DOut1,DOut2,AddrA,AddrB,DataInA,DataInB,AddOut,SUBOut,DOut3);

	output WEB,WEA,IncA,IncB,sign;
	input [7:0] DataInA;
	output [7:0] DOut1;
	output [7:0] DOut2;
	output [2:0] AddrA;
	output [1:0] AddrB;
	output [7:0] AddOut;
	output [7:0] SUBOut;
	input clock,Reset;
	output [7:0]DataInB;
	output [7:0] DOut3;

controller C(.IncA(IncA),.IncB(IncB),.WEA(WEA),.WEB(WEB),.Reset(Reset),.clock(clock));

CounterA counta(.IncA(IncA),.Reset(Reset),.clock(clock),.AddrA(AddrA));

MemoryA mema(.AddrA(AddrA),.WEA(WEA),.DataInA(DataInA),.DOut1(DOut1));

D_ff_project1 Dff(.DOut2(DOut2),.DOut1(DOut1),.clock(clock));

comparator comp(.sign(sign),.DOut2(DOut2),.DOut1(DOut1));

ADD A(.AddOut(AddOut),.DOut1(DOut1),.DOut2(DOut2));

SUB S(.SUBOut(SUBOut),.DOut1(DOut1),.DOut2(DOut2));

MUX M(.AddOut(AddOut),.SUBOut(SUBOut),.sign(sign),.DataInB(DataInB));

CounterB countb(.IncB(IncB),.Reset(Reset),.clock(clock),.AddrB(AddrB));

MemoryB memb(.AddrB(AddrB),.DataInB(DataInB),.WEB(WEB),.DOut3(DOut3));

endmodule

`timescale 10ns/1ns
module CounterA_tb;
	
	reg Reset, IncA, clock;
	wire [2:0]AddrA;
	CounterA MUT(.Reset(Reset), .clock(clock), .IncA(IncA),.AddrA(AddrA));


	initial
	begin
	Reset = 1;
	#5 Reset<=0;
	IncA = 1;
	clock = 0; 
	end

	always
	begin 
	#5 clock = ~clock;
	#7 IncA <= ~IncA;
	end

endmodule 
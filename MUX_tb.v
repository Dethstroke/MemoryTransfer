`timescale 10ns/1ns
module MUX_tb;

reg [7:0]AddOut,SUBOut;
reg Sign;
wire [7:0] DataInB;

MUX mux(.AddOut(AddOut),.SUBOut(SUBOut),.Sign(Sign),.DataInB(DataInB));

initial
begin
	AddOut = 0;
	SUBOut =0;
	Sign = 0;

#2
	AddOut = 9;
  	SUBOut = 10;
  	Sign = 1;
#2
	AddOut = 5;
  	SUBOut = 2;	
	Sign = 0;

#2	 AddOut = 7;
  	SUBOut = 11;
 	 Sign = 1;

end


endmodule


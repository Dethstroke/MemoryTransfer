`timescale 10ns/1ns
module CounterB_tb ();
	
	reg Reset, IncB, clock;
	wire [1:0]AddrB;
	CounterB B1(.Reset(Reset), .clock(clock), .IncB(IncB),.AddrB(AddrB));


	initial
	begin
	clock <= 0; 
	Reset <= 1;
#5	Reset <= 0;
	IncB <= 0;
#10 IncB <= 1;
	end

	always
	begin 
	#5 clock <= ~clock;
	
	end

endmodule 
 
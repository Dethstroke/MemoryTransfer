//verilog code for controller
`timescale 10ns/1ns
module controller(IncA,IncB,WEA,WEB,Reset,clock);

	output IncA,IncB,WEA,WEB;
	input	Reset,clock;
	reg [4:0]nstate;
	reg WEA,WEB,IncA,IncB; 

	parameter s0=5'b00000,s1=5'b00001,s2=5'b00010,s3=5'b00011,s4=5'b00100,s5=5'b00101,s6=5'b00110,s7=5'b00111,
		  s8=5'b01000,s9=5'b01001,s10=5'b01010,s11=5'b01011,s12=5'b01100,s13=5'b01101,s14=5'b01110,s15=5'b01111,
		  s16=5'b10000,s17=5'b10001,s18=5'b10010;
initial
begin
	WEA<=0;
	IncA<=0;
	WEB<=0;
	IncB<=0;
	nstate <= s0;
end
always @(posedge clock or negedge Reset)
	begin
		if(Reset == 1)
		begin
		WEA<=0;
		IncA<=0;
		WEB<=0;
		IncB<=0;
		end
		else
		case (nstate)
		s0:	begin
			WEA<=1;
			IncA<=1;
			nstate <= s1;
			end

		s1:	begin
			WEA<=1;
			IncA<=1;
			nstate <= s2;
			end

		s2:	begin
			nstate <= s3;
			end

		s3:	begin
			nstate <= s4;
			end

		s4:	begin
			nstate <= s5;
			end

		s5:	begin
			nstate <= s6;
			end

		s6:	begin
			nstate <= s7;
			end

		s7:	begin
			nstate <= s8;
			end

		s8:	begin
			nstate <= s9;
			end

		s9:	begin
			WEA<=0;
			nstate <= s10;
			end

		s10:	begin
			nstate <= s11;
		#2	WEB<=1;
			end

		s11:	begin
		#2	WEB<=0;
		#2	IncB<=1;
			nstate <= s12;
			end

		s12:	begin
		#2	WEB<=1;
		#2	IncB<=0;
			nstate <= s13;
			end

		s13:	begin
		#2	WEB<=0;
		#2	IncB<=1;
			nstate <= s14;
			end

		s14:	begin
		#2	WEB<=1;
		#2	IncB<=0;
			nstate <= s15;
			end

		s15:	begin
		#2	WEB<=0;
		#2	IncB<=1;
			nstate <= s16;
			end

		s16:	begin
		#2	WEB<=1;
		#2	IncB<=0;
			nstate <= s17;
			end

		s17:	begin
			WEA<=0;
			IncA<=0;
		#2	WEB<=0;
		#2	IncB<=1;
			nstate <= s18;
			end

		s18:	begin
			WEA<=0;
			IncA<=0;
		#2	WEB<=1;
		#2	IncB<=0;
			nstate <= s0;
			end
		default: begin
				WEA<=0;
				IncA<=0;
				WEB<=0;
				IncB<=0;
				nstate<=s0;
			end
		endcase
	end
endmodule





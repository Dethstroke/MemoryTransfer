//verilog code for comparator

module comparator (sign,DOut2,DOut1);

	input [7:0] DOut1;
	input [7:0] DOut2;
	output sign;	
	reg sign;
	
	initial
	sign = 0;
		
	always @(DOut1,DOut2)
		begin
			if(DOut2 >= DOut1)
				begin
				sign <= 0;
				end	
			else if(DOut2 < DOut1)
				begin
				sign <= 1;
				end
			
		end	 
endmodule

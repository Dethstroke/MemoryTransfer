module D_ff_project1(DOut2,DOut1,clock);

	output [7:0] DOut2;
	input [7:0] DOut1;
	input clock;
	
	reg [7:0] DOut2;

	initial
		begin
		DOut2 <= 8'b0;
		end
	
	always @(posedge clock)
		
			begin
			DOut2 <= DOut1;
			end
		
endmodule

`timescale 10ns/1ns
module ADD_tb;

	reg [7:0]DOut1;
	reg [7:0]DOut2;
	wire [7:0]AddOut;
	

ADD add(.AddOut(AddOut),.DOut1(DOut1),.DOut2(DOut2));

	initial
	begin
	DOut1<=0;DOut2<=0;
	#10 DOut1<=1; DOut2<=5;
	#20 DOut1<=3; DOut2<=7;
	end

endmodule

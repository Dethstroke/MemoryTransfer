//verilog code for counter B)
`timescale 10ns/1ns
module CounterB(IncB,Reset,clock,AddrB);

	output [1:0] AddrB;
	input IncB,Reset,clock;
	reg [1:0] AddrB;

	initial
	begin
	AddrB <= 0;
	end

	always @(posedge clock) 
	begin
	if (Reset == 1)
	begin	
	AddrB <= 0;
	end

	else if (IncB ==1 && Reset==0)
	begin
	AddrB <= AddrB + 1;
	end
end
endmodule

	
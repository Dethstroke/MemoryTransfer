`timescale 10ns/1ns
module MemoryA (AddrA,WEA,DataInA,DOut1);

input WEA;
input [2:0] AddrA;
input [7:0] DataInA;
output [7:0] DOut1;
reg [7:0] DOut1;
reg [7:0] MemoryA [7:0];

always @ (AddrA,WEA)
begin

	if (WEA)
	begin
	MemoryA[AddrA] <= DataInA;
	DOut1<=0;
	end
	else
	begin
	DOut1 <= MemoryA[AddrA];
	end
end
endmodule 
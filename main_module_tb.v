`timescale 10ns/1ns;
module M_M_tb;

	reg clock,Reset;
	wire  WEB,WEA,IncA,IncB,sign;	
	wire [7:0] DOut1;
	wire [7:0] DOut2;
	wire [2:0] AddrA;
	wire [1:0] AddrB;
	wire [7:0] AddOut;
	wire [7:0] SUBOut;
	reg [7:0] DataInA;
	wire [7:0]DataInB;
	wire [7:0] DOut3;

M_M main_mem(.DOut3(DOut3),.WEB(WEB),.WEA(WEA),.IncA(IncA),.IncB(IncB),.sign(sign),.clock(clock),.Reset(Reset),
	.DOut1(DOut1),.DOut2(DOut2),.AddrA(AddrA),.AddrB(AddrB),.DataInA(DataInA),.DataInB(DataInB),.AddOut(AddOut),.SUBOut(SUBOut));
initial
begin
clock=0;
Reset=1;
#5 Reset=0;
DataInA = 0;
end

initial
begin
#15;
@(negedge clock)
	DataInA=5;
@(negedge clock)
	DataInA=3;
@(negedge clock)
	DataInA=4;
@(negedge clock)
	DataInA=8;
@(negedge clock)
	DataInA=6;
@(negedge clock)
	DataInA=7;
@(negedge clock)
	DataInA=11;
@(negedge clock)
	DataInA=9;
#20;
end

always   
  begin
#5 clock= ~ clock;
  end
endmodule

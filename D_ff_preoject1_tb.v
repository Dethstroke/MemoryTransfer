module D_ff_project1_tb;
	
	wire [7:0] DOut2;
	reg [7:0] DOut1;
	reg clock,reset;


	initial begin
		reset <= 1;
		clock <= 0;
		DOut1 <= 8'b00001111;
		end

	always 	#10 clock = ~clock;

D_ff_project1 D1(.DOut2(DOut2),.DOut1(DOut1),.clock(clock),.reset(reset));

endmodule

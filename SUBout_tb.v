
`timescale 10ns/1ns
module sub_tb;

reg [7:0] DOut1;
reg [7:0] DOut2;
wire [7:0] SubOut;


SUB sub(.DOut1(DOut1),.DOut2(DOut2),.SubOut(SubOut));

initial
 begin
DOut1<=0; DOut2<=0;
   #10 DOut1=2;DOut2=5;
    #20 DOut1=3;DOut2=8;
 end
endmodule

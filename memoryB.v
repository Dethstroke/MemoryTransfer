//verilog code for memoryB
module MemoryB(AddrB,DataInB,DOut3,WEB);
	
	output [7:0] DOut3;
	input [1:0] AddrB;
	input [7:0] DataInB;
	reg [7:0] DOut3;
	input WEB;
	reg [7:0] MemoryB [3:0];

always @ (AddrB,WEB)
	begin
	if(WEB)
		begin
		if(AddrB < 4)
		begin
		MemoryB[AddrB] <= DataInB;
		end
		end
	else if(WEB == 0 && AddrB < 4)
		begin
		DOut3 <= MemoryB[AddrB];
		end
end
endmodule 

`timescale 10ns/1ns;
module MemoryA_tb ();
	
	reg WEA, clock;
	reg [7:0] DataInA;
	reg [2:0] AddrA;
	wire [7:0] DOut1;
	reg [8:0] counter;
	MemoryA MUT(.WEA(WEA), .clock(clock), .DataInA(DataInA),.AddrA(AddrA),.DOut1(DOut1));
	
	initial
	begin
	clock<=0;
	WEA<=1;
	AddrA <= 0;
	DataInA<=0;
#10
	AddrA<=1;
	DataInA<=1;
#10	
	AddrA<=2;
	DataInA<=2;
#10	
	AddrA<=3;
	DataInA<=3;
#10	
	AddrA<=4;
	DataInA<=4;
#10	
	AddrA<=5;
	DataInA<=5;
#10	
	AddrA<=6;
	DataInA<=6;
#10	
	AddrA<=7;
	DataInA<=7;
#10	
	WEA<=0;
	AddrA<=0;
#10	
	WEA<=0;
	AddrA<=1;
#10	
	WEA<=0;
	AddrA<=2;
#10	
	WEA<=0;
	AddrA<=3;
#10	
	WEA<=0;
	AddrA<=4;
#10	
	WEA<=0;
	AddrA<=5;
#10	
	WEA<=0;
	AddrA<=6;
#10	
	WEA<=0;
	AddrA<=7;
	end

always #5 clock = ~clock;

endmodule 
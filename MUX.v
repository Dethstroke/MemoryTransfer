
module MUX(AddOut,SUBOut,sign,DataInB);

	output [7:0] DataInB;
	input[7:0] AddOut;	
	input[7:0] SUBOut;	
	input sign;

assign DataInB = sign ? AddOut : SUBOut;

endmodule

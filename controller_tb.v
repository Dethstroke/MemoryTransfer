
`timescale 10ns/1ns
module controller_tb;

	reg Reset,clock;
	wire IncA,IncB,WEA,WEB;

	
	initial begin
	Reset <= 0;
	clock <= 0;	
	end


	always #5 clock = ~clock; 

	controller C1(.IncA(IncA),.IncB(IncB),.WEA(WEA),.WEB(WEB),.Reset(Reset),.clock(clock));
	


endmodule


